package com.snaip4el.simplewords

import com.snaip4el.simplewords.phraseGenerator.*
import com.snaip4el.simplewords.phraseGenerator.generators.*
import org.junit.Assert
import org.junit.Test
import kotlin.random.Random

class GetVerbs {
    lateinit var phraseGenerator: PhraseCreator

    @Test
    fun addition_isCorrect() {
        var verbs = getVerbs()
        val objectSubject = getSubjectObjectNoun().toList()
        val nouns = getNouns()



        for(i in 1.. 20){
            var wordPack = WordPack(objectSubject.shuffled()[0],verbs.shuffled()[0],nouns.shuffled()[0])
            printPhras(ToBePresentPositive(wordPack))
        }

        Assert.assertEquals(4, 2 + 2)
    }


    fun printPhras(phraseGenerator: PhraseCreator){
        val phrases = phraseGenerator.phrase
        println(phraseGenerator.tensAndType.name)
        phrases.forEach({print(it.ansverPart+ " " )})
        println()
        phrases.forEach({println("${it.qestionPart} - ${it.ansverPart}")})
        println("===========================")
    }


}


