package com.snaip4el.simplewords


import io.reactivex.rxjava3.annotations.NonNull
import io.reactivex.rxjava3.core.Observable
import io.reactivex.rxjava3.core.Observer
import io.reactivex.rxjava3.disposables.Disposable
import io.reactivex.rxjava3.subjects.PublishSubject
import org.junit.Assert.assertTrue
import org.junit.Test


/**
 * Example local unit test, which will execute on the development machine (host).
 *
 * See [testing documentation](http://d.android.com/tools/testing).
 */
class ExampleUnitTest {
    var result = ""

    // Simple subscription to a fix value
    @Test
    fun returnAValue() {


        val observable = Observable.just("one", "two", "three")
        observable.subscribe({ println("t1 $it") })

        //Create an Observable//
        val myObservable = getObservable()
        // добаляю два обозревателя


        val publishSubject: PublishSubject<String> = PublishSubject.create()
        val disposable = setObserver1(publishSubject)
        setObserver2(publishSubject)
        publishSubject.onNext("one")


/*        for (i in 1..5){
            Thread.sleep(300)
            publishSubject.onNext("from thred $i")
        }*/


        val ooo = Observable.just("210211").subscribe(getObserver())

        val oooo = Observable.just("210211 2 test").map { it.toUpperCase() }

        oooo.subscribe(getObserver())

        assertTrue("Hello" == "Hello")







    }

    private fun getObserver(): Observer<String> {
        return object : Observer<String> {
            override fun onSubscribe(d: Disposable) {
            }

            override fun onNext(s: String) {
                println("onNext: $s")

            }

            override fun onError(e: Throwable) {
                println("onError: " + e.message)
            }

            override fun onComplete() {
                println("onComplete")
            }
        }
    }


    //Give myObservable some data to emit//
    private fun getObservable(): Observable<String> {
        return Observable.just("1","2","3")
    }

    private fun<T> setObserver1(observable: PublishSubject<T>): @NonNull Disposable? {

        return  observable.subscribe({println("setObserver1 $it")})
    }
    private fun<T> setObserver2(observable: PublishSubject<T>){
        observable.subscribe({println("setObserver2 $it")})

    }





}