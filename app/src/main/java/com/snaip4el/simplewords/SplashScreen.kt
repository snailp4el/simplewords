package com.snaip4el.simplewords

import android.content.Context
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.fragment.app.Fragment
import com.google.android.material.snackbar.Snackbar
import com.snaip4el.simplewords.task_selector.*
import io.reactivex.rxjava3.core.Observable
import io.reactivex.rxjava3.schedulers.Schedulers
import javax.inject.Inject
import javax.inject.Singleton

private const val TAG = "SplashScreen"

@Singleton
class SplashScreen @Inject constructor(
    var taskSelectorViewModel: TasekSelectorViewModel
    ):Fragment() {




    override fun onAttach(context: Context) {
        super.onAttach(context)
        App.component.inject(this)
        Log.i(TAG,"onAttachEnd" )
    }



    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.fragment_splash_screen, container, false)
        return view
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val tv:TextView = view.findViewById(R.id.splash_text)
        val v = taskSelectorViewModel.taskLiveData

        v.observe(
            viewLifecycleOwner,
            {
                tv.setText(it.toString())
            })

        taskSelectorViewModel.getTasks()

    }



}