package com.ogulcan.android.mvp.app.ui.base

import androidx.fragment.app.Fragment

/**
 * Created by ogulcan on 07/02/2018.
 */
class BaseContract {

    interface Presenter<in T> {
        fun attach(view: T)
    }

    interface View {

    }
}