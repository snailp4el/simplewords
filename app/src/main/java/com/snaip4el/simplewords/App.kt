package com.snaip4el.simplewords

import android.app.Application
import android.content.Context
import com.snaip4el.simplewords.di.Component
import com.snaip4el.simplewords.di.DaggerComponent
import com.snaip4el.simplewords.di.Module

class App: Application(){

    companion object{
        lateinit var component: Component
    }


    override fun onCreate() {
        super.onCreate()
        component = DaggerComponent.builder().module(Module(this)).build()
    }
}

/*
val Context.appComponent: Component
    get() = when(this){
        is App -> appComponent
        else -> this.applicationContext.appComponent
    }*/
