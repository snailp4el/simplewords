/*
 * Copyright (C) 2018 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.snaip4el.simplewords.utils

import android.util.Log
import android.view.View
import android.view.animation.Transformation
import android.view.animation.TranslateAnimation
import android.widget.Button
import androidx.databinding.BindingAdapter
import com.snaip4el.simplewords.phraseGenerator.Tens

private val TAG = "@BindingAdapter"


@BindingAdapter("app:hideIfZero")
fun hideIfZero(view: View, number: Int) {
    view.visibility = if (number == 0) View.GONE else View.VISIBLE
}

@BindingAdapter("app:myAdapter2")
fun myAdapter2(button: Button, number: Int) {
    //button.visibility = if (number > 3) View.GONE else View.VISIBLE
    Log.i(TAG, "myAdapter2 $number")
    // get parent width
    val parent = button.parent as View
    val parrenWidth = parent.width
    //button.setText("h")

    // set button width 1/3 parent
    val lp = button.layoutParams
    lp.width = (parrenWidth/3)
    button.layoutParams = lp

    if (number%1 == 0) toLeft(button, parrenWidth)
    if (number%2 == 0) toRight(button, parrenWidth)
    if (number%3 == 0) toCentre(button, parrenWidth)

}

//адаптер который двигает кнопку
@BindingAdapter("app:moveAdapter")
fun moveAdapter(button: Button, tens: Tens?) {
    //button.visibility = if (number > 3) View.GONE else View.VISIBLE
    Log.i(TAG, "moveAdapter $tens")
    // get parent width
    val parent = button.parent as View
    val parrenWidth = parent.width
    //button.setText("h")

    // set button width 1/3 parent
    val lp = button.layoutParams
    lp.width = (parrenWidth/3)
    button.layoutParams = lp

    if (tens == Tens.PAST_POSITIVE || tens == Tens.PAST_NEGATIVE ||tens == Tens.PAST_QUESTION) toLeft(button, parrenWidth)
    if (tens == Tens.FUTURE_POSITIVE||tens == Tens.FUTURE_NEGATIVE||tens == Tens.FUTURE_QUESTION) toRight(button, parrenWidth)
    if (tens == Tens.PRESENT_POSITIVE||tens == Tens.PRESENT_NEGATIVE||tens == Tens.PRESENT_QUESTION) toCentre(button, parrenWidth)

    //todo test


    //button.setAlpha(0f);
    //button.animate().alpha(1f).setDuration(500);
    //button.animate().rotation(360f).setDuration(500);

    if (tens?.name?.contains("POSITIVE", ignoreCase = true) == true) button.setText("+")
    if (tens?.name?.contains("NEGATIVE", ignoreCase = true) == true) button.setText("-")
    if (tens?.name?.contains("QUESTION", ignoreCase = true) == true) button.setText("?")

}



fun toLeft(button: Button ,windowWidth: Int){
    button.animate().x(0.toFloat())
}

fun toCentre(button: Button ,windowWidth: Int){
    //val qqq = button.width /2

    val qqq = windowWidth /6
    button.animate().x((windowWidth/2)-qqq.toFloat())
}

fun toRight(button: Button ,windowWidth: Int){
    //val qq = button.getLeft();
    val buttonWidth = button.width
    Log.i(TAG, "toRight $buttonWidth")
    button.animate().x(windowWidth*0.66.toFloat())
}