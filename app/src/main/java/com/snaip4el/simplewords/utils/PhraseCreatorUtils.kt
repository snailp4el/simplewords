package com.snaip4el.simplewords.utils

import com.snaip4el.simplewords.phraseGenerator.*
import java.util.*


object PhraseCreatorUtils {

    fun createNewQueue(tasks : List<Task>): Queue<PhraseCreator> {

        //create a randome task
        val randomTask = tasks.random()
        val newPhrase = createNewPhrase(randomTask)
        val tensPartOfName = randomTask.name.split("_")[0]

        //create task same prifix (TOBE_PAST_QUESTION here TOBE)
        val sortedTask = tasks.filter { it.name.split("_")[0] == tensPartOfName  }.shuffled()
        val queue : Queue<PhraseCreator> = LinkedList()
        for (task in sortedTask){
            queue.add(task.createPhraseGenerator(newPhrase.pack))
        }
        return queue
    }


    fun createNewPhrase(randomTask: Task): PhraseCreator {
        var verbs = getVerbs()
        val subjects = getSubjectObjectNoun().toList()
        val nouns = getNouns()
        val currentPhraseCreator = randomTask.createPhraseGenerator(WordPack(subjects.shuffled()[0],verbs.shuffled()[0],nouns.shuffled()[0]))
        return currentPhraseCreator

    }


}

