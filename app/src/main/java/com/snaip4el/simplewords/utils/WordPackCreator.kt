package com.snaip4el.simplewords.utils

import com.snaip4el.simplewords.phraseGenerator.WordPack
import com.snaip4el.simplewords.phraseGenerator.getNouns
import com.snaip4el.simplewords.phraseGenerator.getSubjectObjectNoun
import com.snaip4el.simplewords.phraseGenerator.getVerbs
import javax.inject.Inject

// help to create

class RandomWordPackCreatorImpl @Inject constructor() : WordPackCreator {

    private var verbs = getVerbs()
    private val subjects = getSubjectObjectNoun().toList()
    private val nouns = getNouns()

    override fun createRandomWordPack() : WordPack {
        return WordPack(subjects.shuffled()[0],verbs.shuffled()[0],nouns.shuffled()[0])
    }



}

interface WordPackCreator{
    fun createRandomWordPack(): WordPack
}

