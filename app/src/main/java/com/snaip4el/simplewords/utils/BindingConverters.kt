

package com.snaip4el.simplewords.utils
import androidx.databinding.BindingConversion
import com.snaip4el.simplewords.phraseGenerator.PartOfPhrase


object BindingConverters {

    @BindingConversion
    @JvmStatic
    fun convertListWotrdsToString(words: List<PartOfPhrase>?): String? {
        val sb = StringBuilder("")
        if (words != null) {
            for (word in words) {
                if (sb.length > 0) sb.append(" ")
                sb.append(word.ansverPart)
            }
        }
        return sb.toString()
    }
}