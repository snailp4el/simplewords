package com.snaip4el.simplewords.di

import android.content.Context
import com.snaip4el.simplewords.RootActivity
import com.snaip4el.simplewords.ScreanContentVM
import com.snaip4el.simplewords.SplashScreen
import com.snaip4el.simplewords.practice3.PracticeFragment
import dagger.Component
import javax.inject.Singleton

@Singleton
@Component(modules = arrayOf(Module::class, Binding::class))
interface Component {

    fun getContext():Context

    fun inject(taskSelector: SplashScreen)
    fun inject(taskSelector: RootActivity)
    fun inject(practiceFragment: PracticeFragment)
    fun getTaskViewModel(): ScreanContentVM

}