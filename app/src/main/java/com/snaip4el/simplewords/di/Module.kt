package com.snaip4el.simplewords.di

import android.content.Context
import com.snaip4el.simplewords.ScreanContentVM
import com.snaip4el.simplewords.task_selector.OneTaskDao
import com.snaip4el.simplewords.task_selector.TaskDatabase
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module()
class Module(private val context: Context) {

    @Provides
    @Singleton
    fun provideContex():Context = context //короткая функция

    @Provides
    @Singleton
    fun provideTaskDatabase(): TaskDatabase {
        return TaskDatabase.getDatabase(context)
    }

    @Provides
    @Singleton
    fun provideOneTaskDao(taskDatabase: TaskDatabase): OneTaskDao {
        return taskDatabase.taskDao()
    }

    @Provides
    fun provide(): ScreanContentVM {
        return ScreanContentVM()
    }


}