package com.snaip4el.simplewords.di

import com.snaip4el.simplewords.ISpeaker
import com.snaip4el.simplewords.TTSspeaker2
import com.snaip4el.simplewords.utils.RandomWordPackCreatorImpl
import com.snaip4el.simplewords.utils.WordPackCreator
import dagger.Binds
import dagger.Module

@Module
interface Binding {

    @Binds
    fun bindWordPackCreator(randomWordPackCreatorImpl: RandomWordPackCreatorImpl):WordPackCreator

    @Suppress("FunctionName")
    @Binds
    fun bindISpeaker(ttSspeaker2: TTSspeaker2):ISpeaker

}