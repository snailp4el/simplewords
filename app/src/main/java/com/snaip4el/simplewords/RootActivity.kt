package com.snaip4el.simplewords

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.Menu
import android.view.MenuItem
import androidx.fragment.app.Fragment
import com.snaip4el.simplewords.practice3.PracticeFragment
import com.snaip4el.simplewords.practiceChoser.PracticeChooser
import com.snaip4el.simplewords.task_selector.TasksSelector
import javax.inject.Inject

private const val TAG = "MainActivity"

class RootActivity : AppCompatActivity() {

    @Inject lateinit var splashScreen: SplashScreen
    @Inject lateinit var taskSelector: TasksSelector
    @Inject lateinit var practiceChooser: PracticeChooser
    @Inject lateinit var practiceFragment: PracticeFragment

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_root)
        App.component.inject(this)

        val isFragmentContainerEmpty = savedInstanceState == null
        if (isFragmentContainerEmpty) {
            launchFragment(practiceFragment)
        }
    }

    private fun launchFragment(fragment: Fragment) {
        supportFragmentManager
            .beginTransaction()
            .replace(R.id.fragmentContainer, fragment)
            .commit()
    }

    fun launchFragmentWithBack(fragment: Fragment) {
        supportFragmentManager
            .beginTransaction()
            .replace(R.id.fragmentContainer, fragment)
            .addToBackStack( "tag" )
            .commit()
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        getMenuInflater().inflate(R.menu.tool_bar, menu);
        return true;
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when(item.getItemId()){
            R.id.tb_words ->{
                Log.i(TAG, "tb_words")
                return true
            }
            R.id.tb_phrases ->{
                Log.i(TAG, "tb_phrases")
                launchFragmentWithBack(taskSelector)
                return true
            }
        }
        return onOptionsItemSelected(item)
    }


}