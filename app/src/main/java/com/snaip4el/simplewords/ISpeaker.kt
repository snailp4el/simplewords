package com.snaip4el.simplewords

import io.reactivex.rxjava3.subjects.PublishSubject

//interface for all whu can speack 

interface ISpeaker {
    fun say(text:String)
    fun getSpeachStatusSubject(): PublishSubject<SpeakingStatus>
}

enum class SpeakingStatus(){
    onStart, onStop, onError
}