package com.snaip4el.simplewords

import android.speech.tts.TextToSpeech
import android.speech.tts.UtteranceProgressListener
import android.util.Log
import io.reactivex.rxjava3.subjects.PublishSubject
import java.util.*
import javax.inject.Inject
private const val TAG = "TTSspeaker2"
class TTSspeaker2 @Inject constructor(): ISpeaker{

    private var tts: TextToSpeech? = null
    private val speachStatusSubject: PublishSubject<SpeakingStatus> = PublishSubject.create()




    init {
        var ttsLst = TextToSpeech.OnInitListener{
            if (it == TextToSpeech.SUCCESS) {
                // set US English as language for tts
                val result = tts!!.setLanguage(Locale.US)
                if (result == TextToSpeech.LANG_MISSING_DATA || result == TextToSpeech.LANG_NOT_SUPPORTED) {
                    Log.e("TTS","The Language specified is not supported!")
                } else {
                }

            } else {
                Log.e("TTS", "Initilization Failed!")
            }
        }
        tts = TextToSpeech(App.component.getContext(), ttsLst)

        if (tts != null) {
            tts!!.setSpeechRate(0.7f)

            tts!!.setOnUtteranceProgressListener(object : UtteranceProgressListener() {
                override fun onStart(p0: String?) {}

                override fun onDone(p0: String?) {
                     Log.i(TAG, "onDone " + p0)
                    speachStatusSubject.onNext(SpeakingStatus.onStop)
                }

                override fun onError(p0: String?) {}
            })
        }


    }

    override fun getSpeachStatusSubject(): PublishSubject<SpeakingStatus> {
        return speachStatusSubject
    }

    private fun speak(text:String){
        tts!!.speak(text, TextToSpeech.QUEUE_FLUSH, null,"")
    }

    override fun say(text: String) {
        speak(text)
    }

}