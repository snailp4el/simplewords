package com.snaip4el.simplewords.practiceChoser

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.snaip4el.simplewords.R
import com.snaip4el.simplewords.task_selector.OneTask
import com.snaip4el.simplewords.task_selector.TasksSelectorAdapter
import io.reactivex.rxjava3.core.Observer
import io.reactivex.rxjava3.disposables.Disposable
import javax.inject.Inject

private const val TAG = "PracticeChooser"

class PracticeChooser @Inject constructor(): Fragment(){


    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.tasks_selector_host, container, false)

        val list = getModelList(view.context)

        // set up the RecyclerView
        val recyclerView = view.findViewById<RecyclerView>(R.id.task_selector_host)
        recyclerView.layoutManager = LinearLayoutManager(view.context)

        val observer:Observer<Intent> = object : Observer<Intent> {
            override fun onSubscribe(d: Disposable?) {}
            override fun onNext(i: Intent?) {
                Log.i(TAG,"Click onNext ${i.hashCode()}")
                startActivity(i)
            }
            override fun onError(e: Throwable?) {}
            override fun onComplete() {}

        }
        val adapter = TasksSelectorAdapter(list as ArrayList<Model> , observer)
        recyclerView.adapter = adapter

        return view
    }


}