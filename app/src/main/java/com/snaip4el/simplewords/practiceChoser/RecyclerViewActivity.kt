package com.snaip4el.simplewords.practiceChoser

import android.content.Intent
import android.os.Bundle
import android.util.Log
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.snaip4el.simplewords.R
import io.reactivex.rxjava3.core.Observer
import io.reactivex.rxjava3.disposables.Disposable

//todo to delete

class RecyclerViewActivity :  AppCompatActivity() {

    private val TAG = "RecyclerViewActivity"

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.tasks_selector_host)
    }

    override fun onResume() {
        super.onResume()

        val list = getModelList(this)

        // set up the RecyclerView
        val recyclerView = findViewById<RecyclerView>(R.id.task_selector_host)
        recyclerView.layoutManager = LinearLayoutManager(this)

        val observer:Observer<Intent> = object : Observer<Intent> {
            override fun onSubscribe(d: Disposable?) {}
            override fun onNext(i: Intent?) {
                Log.i(TAG,"Click onNext ${i.hashCode()}")
                startActivity(i)
            }
            override fun onError(e: Throwable?) {}
            override fun onComplete() {}

        }
        val adapter = TasksSelectorAdapter(list as ArrayList<Model> , observer)
        recyclerView.adapter = adapter

    }
}

