package com.snaip4el.simplewords.practiceChoser

import android.content.Intent
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.snaip4el.simplewords.R
import io.reactivex.rxjava3.core.Observable
import io.reactivex.rxjava3.core.Observer

class TasksSelectorAdapter(private val modelArray: ArrayList<Model>, val observer: Observer<Intent>):  RecyclerView.Adapter<TasksSelectorAdapter.ViewHolder>() {
    private val TAG = "TasksSelectorAdapter"
    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val name: TextView

        init {
            // Define click listener for the ViewHolder's View.
            name = view.findViewById(R.id.task_name) as TextView
        }
    }

    // Create new views (invoked by the layout manager)
    override fun onCreateViewHolder(viewGroup: ViewGroup, viewType: Int): ViewHolder {
        // Create a new view, which defines the UI of the list item
        val view = LayoutInflater.from(viewGroup.context)
            .inflate(R.layout.practice_selector_row_item, viewGroup, false)

        return ViewHolder(view)
    }

    // Replace the contents of a view (invoked by the layout manager)
    override fun onBindViewHolder(viewHolder: ViewHolder, position: Int) {
        // Get element from your dataset at this position and replace the
        // contents of the view with that element
        viewHolder.name.text = modelArray[position].name
        viewHolder.name.setOnClickListener({
            Log.i(TAG, "click modelArray")
            var observable = Observable.just(modelArray[position].intent).subscribe(observer)

        })

    }

    // Return the size of your dataset (invoked by the layout manager)
    override fun getItemCount() = modelArray.size

}