package com.snaip4el.simplewords.task_selector


import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import io.reactivex.rxjava3.core.Flowable




@Dao()
interface OneTaskDao {

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    fun insertAllIgnors(tasks: List<OneTask>)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertAllReplace(tasks: List<OneTask>)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertAllReplaceMutableList(tasks: MutableList<OneTask>)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun addTask (task:OneTask)

    @Query("SELECT * FROM task_table")
    fun readAll(): List<OneTask>

    @Query("SELECT * FROM task_table")
    fun readAllMutableList(): MutableList<OneTask>

    @Query("SELECT * FROM task_table")
    suspend fun readAlls(): List<OneTask>



}