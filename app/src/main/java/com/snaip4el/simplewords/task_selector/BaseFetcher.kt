package com.snaip4el.simplewords.task_selector

import com.snaip4el.simplewords.App
import com.snaip4el.simplewords.phraseGenerator.Task
import io.reactivex.rxjava3.core.Observable
//todo delete it
object BaseFetcher {

    fun getTasksFromDb(): Observable<List<Task>> {

        //get all task wich we have in enum class Task
        val oneTaskFromEnumTask = Task.values().map{ OneTask(it.toString()) }

        val create = Observable.create<List<Task>>({
            var db: TaskDatabase = TaskDatabase.getDatabase(App.component.getContext())
            var dao: OneTaskDao = db.taskDao()
            dao.insertAllIgnors(oneTaskFromEnumTask)// if we add new TaskCreater it will add in base them
            var taskFromBase = dao.readAll()
            var tasks = taskFromBase.filter { it.show == true }.map { enumValueOf<com.snaip4el.simplewords.phraseGenerator.Task>(it.task) }

            if (tasks.size <= 0)tasks = oneTaskFromEnumTask //add all phrasele if tassk emphty
                    .map { enumValueOf<com.snaip4el.simplewords.phraseGenerator.Task>(it.task) }

            it.onNext(tasks)
        })
        return create
    }

}