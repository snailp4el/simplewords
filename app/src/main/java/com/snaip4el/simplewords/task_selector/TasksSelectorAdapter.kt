package com.snaip4el.simplewords.task_selector

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.CheckBox
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.snaip4el.simplewords.R

class TasksSelectorAdapter(private val tasks: ArrayList<OneTask>):  RecyclerView.Adapter<TasksSelectorAdapter.ViewHolder>() {

    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val taskName: TextView
        val showCB: CheckBox
        val frequency: TextView

        init {
            showCB = view.findViewById(R.id.show_cb) as CheckBox
            taskName = view.findViewById(R.id.task_name) as TextView
            frequency = view.findViewById(R.id.frequency) as TextView
            frequency.setVisibility(View.INVISIBLE)
        }
    }

    // Create new views (invoked by the layout manager)
    override fun onCreateViewHolder(viewGroup: ViewGroup, viewType: Int): ViewHolder {
        // Create a new view, which defines the UI of the list item
        val view = LayoutInflater.from(viewGroup.context)
                .inflate(R.layout.tasks_selector_row_item, viewGroup, false)

        return ViewHolder(view)
    }

    // Replace the contents of a view (invoked by the layout manager)
    override fun onBindViewHolder(viewHolder: ViewHolder, position: Int) {
        // Get element from your dataset at this position and replace the
        // contents of the view with that element
        viewHolder.taskName.text = tasks[position].task.toString()
        viewHolder.frequency.text = tasks[position].frequency.toString()
        viewHolder.showCB.setChecked(tasks[position].show)
        viewHolder.showCB.setOnClickListener{ tasks[position].show = viewHolder.showCB.isChecked }
    }

    // Return the size of your dataset (invoked by the layout manager)
    override fun getItemCount() = tasks.size


}