package com.snaip4el.simplewords.task_selector
import androidx.room.Entity
import androidx.room.PrimaryKey
import androidx.room.TypeConverter
import androidx.room.TypeConverters
import com.snaip4el.simplewords.phraseGenerator.Task
import com.snaip4el.simplewords.phraseGenerator.TaskConverter


@Entity(tableName = "task_table")
data class OneTask(
    @PrimaryKey
    val task: String,
    var show: Boolean = true,
    var frequency: Int = 1)
