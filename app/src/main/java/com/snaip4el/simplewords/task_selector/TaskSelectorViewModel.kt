package com.snaip4el.simplewords.task_selector

import android.content.Context
import android.os.AsyncTask
import android.util.Log
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.snaip4el.simplewords.phraseGenerator.Task
import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers
import io.reactivex.rxjava3.core.Observable
import io.reactivex.rxjava3.functions.Consumer
import io.reactivex.rxjava3.schedulers.Schedulers
import javax.inject.Inject
import javax.inject.Singleton
import kotlin.random.Random

private const val TAG = "TasekSelectorViewModel"
@Singleton
class TasekSelectorViewModel @Inject constructor(
    var dao: OneTaskDao,
    var context: Context
    ):  ViewModel() {

    var taskLiveData: MutableLiveData<MutableList<OneTask>> = MutableLiveData()

    fun getTasks(){
        Observable.fromCallable {
            val oneTaskFromEnumTask = Task.values().map{OneTask(it.toString())}
            dao.insertAllIgnors(oneTaskFromEnumTask)
            taskLiveData.postValue(dao.readAllMutableList())
        }
            .subscribeOn(Schedulers.computation())
            .subscribe()
    }

    fun putTasksInBase(){
        Observable.fromCallable {
            if(taskLiveData.value?.filter { it.show }?.size == 0){
                taskLiveData.value?.map { it.show = true }
                Log.i(TAG, "not Jobs not selected ")
            }
            dao.insertAllReplaceMutableList(taskLiveData.value?: mutableListOf()) }
            .subscribeOn(Schedulers.computation())
            .subscribe()
    }
}