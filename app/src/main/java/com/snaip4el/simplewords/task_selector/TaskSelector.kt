package com.snaip4el.simplewords.task_selector

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.snaip4el.simplewords.R
import javax.inject.Inject
import kotlin.collections.ArrayList

private const val TAG = "TasksSelector"

class TasksSelector @Inject constructor(var taskSelectorViewModel: TasekSelectorViewModel): Fragment() {

    lateinit var adapter: TasksSelectorAdapter

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.tasks_selector_host, container, false)

        val recyclerView = view.findViewById<RecyclerView>(R.id.task_selector_host)
        recyclerView.layoutManager = LinearLayoutManager(view.context)


        taskSelectorViewModel.taskLiveData.observe(
            viewLifecycleOwner,
            {
                adapter = TasksSelectorAdapter(it as ArrayList<OneTask>)
                recyclerView.adapter = adapter
            })

        taskSelectorViewModel.getTasks()
        return view
    }



    override fun onPause() {
        super.onPause()
        taskSelectorViewModel.putTasksInBase()
    }


}

