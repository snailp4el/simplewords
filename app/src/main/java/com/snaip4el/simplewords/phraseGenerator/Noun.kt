package com.snaip4el.simplewords.phraseGenerator

class Noun(word: String, translation: String,complexity:Int, val plural: String, val calculated:Boolean = true, val mayBePerson:Boolean = true) :
    Word(word, translation, complexity) {
}