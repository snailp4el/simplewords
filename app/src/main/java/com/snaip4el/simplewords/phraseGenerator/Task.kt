package com.snaip4el.simplewords.phraseGenerator

import androidx.room.TypeConverter
import com.snaip4el.simplewords.phraseGenerator.generators.*

enum class Task {

    SIMPLE_PRESENT_POSITIVE
    { override fun createPhraseGenerator(pack: WordPack): PhraseCreator { return SimplePresentPositive(pack) } },
    SIMPLE_PRESENT_NEGATIVE
    { override fun createPhraseGenerator(pack: WordPack): PhraseCreator { return SimplePresentNegative(pack) } },
    SIMPLE_PRESENT_QUESTION
    { override fun createPhraseGenerator(pack: WordPack): PhraseCreator { return SimplePresentQuestion(pack) } },
    SIMPLE_FUTURE_POSITIVE
    { override fun createPhraseGenerator(pack: WordPack): PhraseCreator { return SimpleFuturePositive(pack) } },
    SIMPLE_FUTURE_NEGATIVE
    { override fun createPhraseGenerator(pack: WordPack): PhraseCreator { return SimpleFutureNegative(pack) } },
    SIMPLE_FUTURE_QUESTION
    { override fun createPhraseGenerator(pack: WordPack): PhraseCreator { return SimpleFutureQuestion(pack) } },
    SIMPLE_PAST_POSITIVE
    { override fun createPhraseGenerator(pack: WordPack): PhraseCreator { return SimplePastPositive(pack) } },
    SIMPLE_PAST_NEGATIVE
    { override fun createPhraseGenerator(pack: WordPack): PhraseCreator { return SimplePastNegative(pack) } },
    SIMPLE_PAST_QUESTION
    { override fun createPhraseGenerator(pack: WordPack): PhraseCreator { return SimplePastQuestion(pack) } },
    TOBE_PRESENT_POSITIVE
    { override fun createPhraseGenerator(pack: WordPack): PhraseCreator { return ToBePresentPositive(pack) } },
    TOBE_PRESENT_NEGATIVE
    { override fun createPhraseGenerator(pack: WordPack): PhraseCreator { return ToBePresentNegative(pack) } },
    TOBE_PRESENT_QUESTION
    { override fun createPhraseGenerator(pack: WordPack): PhraseCreator { return ToBePresentQuestion(pack) } },
    TOBE_FUTURE_POSITIVE
    { override fun createPhraseGenerator(pack: WordPack): PhraseCreator { return ToBeFuturePosetive(pack) } },
    TOBE_FUTURE_NEGATIVE
    { override fun createPhraseGenerator(pack: WordPack): PhraseCreator { return ToBeFutureNegative(pack) } },
    TOBE_FUTURE_QUESTION
    { override fun createPhraseGenerator(pack: WordPack): PhraseCreator { return ToBeFutureQuestion(pack) } },
    TOBE_PAST_POSITIVE
    { override fun createPhraseGenerator(pack: WordPack): PhraseCreator { return ToBePastPositive(pack) } },
    TOBE_PAST_NEGATIVE
    { override fun createPhraseGenerator(pack: WordPack): PhraseCreator { return ToBePastNegative(pack) } },
    TOBE_PAST_QUESTION
    { override fun createPhraseGenerator(pack: WordPack): PhraseCreator { return ToBePastQuestion(pack) } },
    ;

    abstract fun createPhraseGenerator(pack: WordPack):PhraseCreator
}

class TaskConverter {

    @TypeConverter
    fun toTask(value: String) = enumValueOf<Task>(value)

    @TypeConverter
    fun fromTask(value: Task) = value.name

}