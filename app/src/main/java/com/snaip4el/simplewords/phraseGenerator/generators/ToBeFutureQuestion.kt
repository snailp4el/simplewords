package com.snaip4el.simplewords.phraseGenerator.generators

import com.snaip4el.simplewords.phraseGenerator.*

class ToBeFutureQuestion (pack: WordPack) : PhraseCreator(pack) {
    override val tensAndType = Tens.FUTURE_QUESTION



    init {
        addPart("will")
        if (pack.subjectNoun.article != "") addPart(pack.subjectNoun.article, pack.subjectNoun.article)
        addPart(pack.subjectNoun.word, pack.subjectNoun.word)
        addPart("be")
        if (pack.subjectNoun.singular|| pack.subjectNoun.word.toLowerCase() == "you"){
            addPart("a")
            addPart(pack.nounPerson.word+"?", pack.nounPerson.word)
        }else{
            addPart(pack.nounPerson.plural+"?", pack.nounPerson.word)
        }

    }




}