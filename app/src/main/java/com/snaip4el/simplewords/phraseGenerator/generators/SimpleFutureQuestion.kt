package com.snaip4el.simplewords.phraseGenerator.generators

import com.snaip4el.simplewords.phraseGenerator.*


class SimpleFutureQuestion (pack: WordPack) : PhraseCreator(pack)  {
    override val tensAndType = Tens.FUTURE_QUESTION
    init {

            addPart("will")
            addPart(pack.subjectNoun.articleWord(), pack.subjectNoun.articleWord())
            addPart(pack.verb.word+ "?", pack.verb.word)

    }





}