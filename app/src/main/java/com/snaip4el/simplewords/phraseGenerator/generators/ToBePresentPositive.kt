package com.snaip4el.simplewords.phraseGenerator.generators

import com.snaip4el.simplewords.phraseGenerator.*

class ToBePresentPositive (pack: WordPack) : PhraseCreator(pack) {
    override val tensAndType = Tens.PRESENT_POSITIVE
    init {

        when{
            pack.subjectNoun.word == "i" ||pack.subjectNoun.word ==  "I" ->{//i am
                addPart(pack.subjectNoun.word, pack.subjectNoun.word)
                addPart("am")
                addPart("a")
                addPart(pack.nounPerson.word, pack.nounPerson.word)
            }
            pack.subjectNoun.singular ->{
                addPart(pack.subjectNoun.articleWord(), pack.subjectNoun.articleWord())
                addPart("is")
                addPart("a")
                addPart(pack.nounPerson.word, pack.nounPerson.word)
            }
            !pack.subjectNoun.singular ->{
                addPart(pack.subjectNoun.articleWord(), pack.subjectNoun.articleWord())
                addPart("are")
                addPart(pack.nounPerson.plural, pack.nounPerson.word)
            }

        }

    }




}