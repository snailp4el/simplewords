package com.snaip4el.simplewords.phraseGenerator
// о ком идет речь он, они, кони,
class SubjectObjectNoun(word: String, translation: String, complexity:Int, val singular: Boolean, val article: String = "the") :
    Word(word, translation, complexity) {

        fun articleWord(): String{
            if (article ==""){
                return word
            }
            return article + " " + word
        }


}