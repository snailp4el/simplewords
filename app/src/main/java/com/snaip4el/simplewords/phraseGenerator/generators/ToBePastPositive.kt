package com.snaip4el.simplewords.phraseGenerator.generators

import com.snaip4el.simplewords.phraseGenerator.*

class ToBePastPositive (pack: WordPack) : PhraseCreator(pack) {
    override val tensAndType = Tens.PAST_POSITIVE
    init {

        when{
            pack.subjectNoun.word.toLowerCase() == "you"  ->{//i am
                addPart(pack.subjectNoun.articleWord(), pack.subjectNoun.articleWord())
                addPart("were")
                addPart("a")
                addPart(pack.nounPerson.word, pack.nounPerson.word)
            }
            pack.subjectNoun.singular  ->{
                addPart(pack.subjectNoun.articleWord(), pack.subjectNoun.articleWord())
                addPart("was")
                addPart("a")
                addPart(pack.nounPerson.word, pack.nounPerson.word)
            }
            !pack.subjectNoun.singular ->{
                addPart(pack.subjectNoun.articleWord(), pack.subjectNoun.articleWord())
                addPart("were")
                addPart(pack.nounPerson.plural, pack.nounPerson.word)
            }

        }

    }




}