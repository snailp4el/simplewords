package com.snaip4el.simplewords.phraseGenerator


abstract class PhraseCreator (val pack: WordPack){
    abstract val tensAndType : Tens
    val phrase = arrayListOf<PartOfPhrase>()
    var actionNeadToBeDone = ActionNeadToBeDone.question

    //simple vay add phrase in phrase array
    fun addPart(ansverPart:String, qestionPart:String = ""){
        phrase.add(PartOfPhrase(ansverPart, qestionPart))
    }

    fun getAnsverSring(): String{
        return phrase.fold(""){qq, ww -> (qq + ww.ansverPart+ " ")}
    }

    fun getQuestionSring(): String{
        return phrase.fold("\n"){qq, ww -> (if (ww.qestionPart != "") qq + ww.qestionPart+ "\n" else qq)}
    }

    enum class ActionNeadToBeDone(){
        question, answer, doneWithIt
    }

}

