package com.snaip4el.simplewords.phraseGenerator.generators

import com.snaip4el.simplewords.phraseGenerator.*


class SimplePresentPositive (pack: WordPack) : PhraseCreator(pack)  {
    override val tensAndType = Tens.PRESENT_POSITIVE
    init {
        if(pack.subjectNoun.singular && pack.subjectNoun.articleWord().toLowerCase() != "i"){
            addPart(pack.subjectNoun.articleWord(), pack.subjectNoun.articleWord())
            addPart(pack.verb.sEnding, pack.verb.word)
        }else{
            addPart(pack.subjectNoun.articleWord(), pack.subjectNoun.articleWord())
            addPart(pack.verb.word, pack.verb.word)
        }
    }




}