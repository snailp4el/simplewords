package com.snaip4el.simplewords.phraseGenerator.generators

import com.snaip4el.simplewords.phraseGenerator.*


class SimplePresentNegative (pack: WordPack) : PhraseCreator(pack)  {
    override val tensAndType = Tens.PRESENT_NEGATIVE

    init {
        if(pack.subjectNoun.singular && pack.subjectNoun.articleWord().toLowerCase() != "i"){
            addPart(pack.subjectNoun.articleWord(), pack.subjectNoun.articleWord())
            addPart("does not")
            addPart(pack.verb.word, pack.verb.word)
        }else{
            addPart(pack.subjectNoun.articleWord(), pack.subjectNoun.articleWord())
            addPart("do not")
            addPart(pack.verb.word, pack.verb.word)
        }
    }

}