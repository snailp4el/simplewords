package com.snaip4el.simplewords.phraseGenerator.generators

import com.snaip4el.simplewords.phraseGenerator.*


class SimplePastNegative (pack: WordPack) : PhraseCreator(pack)  {
    override val tensAndType = Tens.PAST_NEGATIVE

    init {
            addPart(pack.subjectNoun.articleWord(), pack.subjectNoun.articleWord())
            addPart("did not")
            addPart(pack.verb.word, pack.verb.word)
    }

}