package com.snaip4el.simplewords.phraseGenerator.generators

import com.snaip4el.simplewords.phraseGenerator.*

class ToBePastNegative (pack: WordPack) : PhraseCreator(pack) {
    override val tensAndType = Tens.PAST_NEGATIVE
    init {

        when{
            pack.subjectNoun.word.toLowerCase() == "you"  ->{
                addPart(pack.subjectNoun.articleWord(), pack.subjectNoun.articleWord())
                addPart("were")
                addPart("not")
                addPart("a")
                addPart(pack.nounPerson.word, pack.nounPerson.word)
            }
            pack.subjectNoun.singular  ->{
                addPart(pack.subjectNoun.articleWord(), pack.subjectNoun.articleWord())
                addPart("was")
                addPart("not")
                addPart("a")
                addPart(pack.nounPerson.word, pack.nounPerson.word)
            }
            !pack.subjectNoun.singular ->{
                addPart(pack.subjectNoun.articleWord(), pack.subjectNoun.articleWord())
                addPart("were")
                addPart("not")
                addPart(pack.nounPerson.plural, pack.nounPerson.word)
            }

        }

    }




}