package com.snaip4el.simplewords.phraseGenerator.generators

import com.snaip4el.simplewords.phraseGenerator.*

class ToBePastQuestion (pack: WordPack) : PhraseCreator(pack) {
    override val tensAndType = Tens.PAST_QUESTION
    init {

        when{
            pack.subjectNoun.word.toLowerCase() == "you"  ->{
                addPart("were")
                addPart(pack.subjectNoun.articleWord(), pack.subjectNoun.articleWord())
                addPart("a")
                addPart(pack.nounPerson.word + "?", pack.nounPerson.word)
            }
            pack.subjectNoun.singular  ->{
                addPart("was")
                addPart(pack.subjectNoun.articleWord(), pack.subjectNoun.articleWord())
                addPart("a")
                addPart(pack.nounPerson.word + "?", pack.nounPerson.word)
            }
            !pack.subjectNoun.singular ->{
                addPart("were")
                addPart(pack.subjectNoun.articleWord(), pack.subjectNoun.articleWord())
                addPart(pack.nounPerson.plural + "?", pack.nounPerson.word)
            }

        }

    }




}