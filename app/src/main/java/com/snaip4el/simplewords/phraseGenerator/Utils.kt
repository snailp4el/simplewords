package com.snaip4el.simplewords.phraseGenerator

    fun getArticles():List<Article>{
        val articles = arrayListOf<Article>()
        articles.add(Article("the", "этот, эти", 0))
        return articles
    }

    fun getNouns(): List<Noun>{
        val nouns = arrayListOf<Noun>()
        nouns.add(Noun("girl", "девочка",0, "girls",true,true))
        nouns.add(Noun("boy", "мальчик",0, "boys",true,true))
        nouns.add(Noun("fireman", "пожарный",0, "firemans",true,true))
        return nouns
    }

    fun getVerbs(): List<Verb>{
        val verbs = arrayListOf<Verb>()
        verbs.add(Verb("work","работать",0,true, "worked","worked",))
        verbs.add(Verb("like","любить"))
        verbs.add(Verb("want","любить",0,true,"wanted","wanted","wants","wanting"))
        verbs.add(Verb("help","помогать",0,true,"helped","helped","helps","helping"))
        verbs.add(Verb("ask","спрашивать",0,true,"asked","asked","asks","asking"))
        verbs.add(Verb("play","играть",0,true,"played","played","plays","playing"))
        verbs.add(Verb("have","иметь",0,false,"had","had","has","having"))
        verbs.add(Verb("say","сказать",0,false,"said","said","says","saying"))
        verbs.add(Verb("go","идти",0,false," went","gone","goes","going"))

        return verbs
    }

    fun getSubjectObjectNoun(): Set<SubjectObjectNoun>{
        val setSON = mutableSetOf<SubjectObjectNoun>()
        setSON.add(SubjectObjectNoun("I","Я",0,true,""))
        setSON.add(SubjectObjectNoun("you","Вы",0,false,""))
        setSON.add(SubjectObjectNoun("dogs","собаки",0,false))
        setSON.add(SubjectObjectNoun("dog","собака",0,true))
        setSON.add(SubjectObjectNoun("cats","кошки",0,false))
        setSON.add(SubjectObjectNoun("they","они",0,false,""))


        setSON.add(SubjectObjectNoun("cat","кошка",0,true))
        setSON.add(SubjectObjectNoun("he","он",0,true,""))
        setSON.add(SubjectObjectNoun("she","она",0,true,""))
        setSON.add(SubjectObjectNoun("it","оно",0,true,""))

        return setSON
    }


    fun tensToSimbol(tens:Tens):String{
        when (tens){
            Tens.PAST_POSITIVE -> return "+"
            Tens.PAST_NEGATIVE  -> return "-"
            Tens.PAST_QUESTION  -> return "?"

            Tens.PRESENT_POSITIVE -> return "+"
            Tens.PRESENT_NEGATIVE -> return "-"
            Tens.PRESENT_QUESTION -> return "?"

            Tens.FUTURE_POSITIVE -> return  "+"
            Tens.FUTURE_NEGATIVE -> return  "-"
            Tens.FUTURE_QUESTION -> return  "?"
        }


    }