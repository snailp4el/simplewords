package com.snaip4el.simplewords.phraseGenerator.generators

import com.snaip4el.simplewords.phraseGenerator.*


class SimplePastQuestion (pack: WordPack) : PhraseCreator(pack)  {
    override val tensAndType = Tens.PAST_QUESTION
    init {

            addPart("did")
            addPart(pack.subjectNoun.articleWord(), pack.subjectNoun.articleWord())
            addPart(pack.verb.word+ "?", pack.verb.word)

    }





}