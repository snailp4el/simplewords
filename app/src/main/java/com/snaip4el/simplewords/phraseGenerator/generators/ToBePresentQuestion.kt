package com.snaip4el.simplewords.phraseGenerator.generators

import com.snaip4el.simplewords.phraseGenerator.*

class ToBePresentQuestion (pack: WordPack) : PhraseCreator(pack) {
    override val tensAndType = Tens.PRESENT_QUESTION
    init {

        when{
            pack.subjectNoun.word == "i" ||pack.subjectNoun.word ==  "I" ->{//i am
                addPart("am")
                addPart(pack.subjectNoun.word, pack.subjectNoun.word)
                addPart("a")
                addPart(pack.nounPerson.word+"?", pack.nounPerson.word)

            }
            pack.subjectNoun.singular ->{
                addPart("is")
                addPart(pack.subjectNoun.articleWord(), pack.subjectNoun.articleWord())
                addPart("a")
                addPart(pack.nounPerson.word+"?", pack.nounPerson.word)
            }
            !pack.subjectNoun.singular ->{
                addPart("are")
                addPart(pack.subjectNoun.articleWord(), pack.subjectNoun.articleWord())
                addPart(pack.nounPerson.plural+"?", pack.nounPerson.word)
            }

        }

    }




}