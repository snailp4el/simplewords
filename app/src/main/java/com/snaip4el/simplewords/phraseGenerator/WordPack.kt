package com.snaip4el.simplewords.phraseGenerator

data class WordPack(val subjectNoun: SubjectObjectNoun,
                    val verb:Verb,
                    val nounPerson: Noun,
                    val article:Article = Article("the","определенный артикль",0)

                    )