package com.snaip4el.simplewords.phraseGenerator.generators

import com.snaip4el.simplewords.phraseGenerator.*


class SimplePastPositive (pack: WordPack) : PhraseCreator(pack)  {
    override val tensAndType = Tens.PAST_POSITIVE
    init {

            addPart(pack.subjectNoun.articleWord(), pack.subjectNoun.articleWord())
            addPart(pack.verb.pastForm, pack.verb.word)

    }




}