package com.snaip4el.simplewords.phraseGenerator.generators

import com.snaip4el.simplewords.phraseGenerator.*


class SimpleFuturePositive (pack: WordPack) : PhraseCreator(pack)  {
    override val tensAndType = Tens.FUTURE_POSITIVE
    init {

            addPart(pack.subjectNoun.articleWord(), pack.subjectNoun.articleWord())
            addPart("will")
            addPart(pack.verb.word, pack.verb.word)

    }




}