package com.snaip4el.simplewords.phraseGenerator.generators

import com.snaip4el.simplewords.phraseGenerator.*

class SimpleFutureNegative (pack: WordPack) : PhraseCreator(pack)  {
    override val tensAndType = Tens.FUTURE_NEGATIVE

    init {
            addPart(pack.subjectNoun.articleWord(), pack.subjectNoun.articleWord())
            addPart("will not")
            addPart(pack.verb.word, pack.verb.word)
    }

}