package com.snaip4el.simplewords.phraseGenerator.generators

import com.snaip4el.simplewords.phraseGenerator.*

class ToBePresentNegative (pack: WordPack) : PhraseCreator(pack) {
    override val tensAndType = Tens.PRESENT_NEGATIVE
    init {

        when{
            pack.subjectNoun.word == "i" ||pack.subjectNoun.word ==  "I" ->{//i am
                addPart(pack.subjectNoun.word, pack.subjectNoun.word)
                addPart("am")
                addPart("not")
                addPart("a")
                addPart(pack.nounPerson.word, pack.nounPerson.word)
            }
            pack.subjectNoun.singular ->{
                addPart(pack.subjectNoun.articleWord(), pack.subjectNoun.articleWord())
                addPart("is")
                addPart("not")
                addPart("a")
                addPart(pack.nounPerson.word, pack.nounPerson.word)
            }
            !pack.subjectNoun.singular ->{
                addPart(pack.subjectNoun.articleWord(), pack.subjectNoun.articleWord())
                addPart("are")
                addPart("not")
                addPart(pack.nounPerson.plural, pack.nounPerson.word)
            }

        }

    }




}