package com.snaip4el.simplewords.phraseGenerator.generators

import com.snaip4el.simplewords.phraseGenerator.*


class SimplePresentQuestion (pack: WordPack) : PhraseCreator(pack)  {
    override val tensAndType = Tens.PRESENT_QUESTION
    init {
        if(pack.subjectNoun.singular && pack.subjectNoun.articleWord().toLowerCase() != "i"){
            addPart("does")
            addPart(pack.subjectNoun.articleWord(), pack.subjectNoun.articleWord())
            addPart(pack.verb.word+ "?", pack.verb.word)
        }else{
            addPart("do")
            addPart(pack.subjectNoun.articleWord(), pack.subjectNoun.articleWord())
            addPart(pack.verb.word+ "?", pack.verb.word)
        }
    }





}