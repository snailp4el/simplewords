package com.snaip4el.simplewords.phraseGenerator

abstract class Word (val word:String, val translation:String, val complexity:Int = 0){

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as Word

        if (word != other.word) return false
        if (translation != other.translation) return false

        return true
    }

    override fun hashCode(): Int {
        var result = word.hashCode()
        result = 31 * result + translation.hashCode()
        return result
    }
}