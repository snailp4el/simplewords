package com.snaip4el.simplewords

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.snaip4el.simplewords.phraseGenerator.*

private const val TAG = "ScreanContentVM"
open class ScreanContentVM {

    val _answer =  MutableLiveData("")
    val answer: LiveData<String> = _answer

    val _question =  MutableLiveData("")
    val question: LiveData<String> = _question

    val _tensAndType = MutableLiveData(Tens.PAST_POSITIVE)
    val tensAndType: LiveData<Tens> = _tensAndType


}