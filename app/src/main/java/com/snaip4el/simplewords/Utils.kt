package com.snaip4el.simplewords

import com.snaip4el.simplewords.phraseGenerator.*
import com.snaip4el.simplewords.phraseGenerator.generators.*

//          0 1   2     3     4     5   6
    val s = "ask|asks|asked|asks|asking|1|спрашивать\n" +
            "help|helps|helped|helps|helping|1|помогать\n" +
            "know|knews|knew|known|knowing|1|знать\n" +
            "get|gets|got|gotten|gotting|1|получать\n" +
            "want|wants|wanted|wanted|wanting|1|хотеть\n" +
            "think|thinks|thought|thought|thinking|1|думать\n" +
            "go|goes|went|gone|going|1|идти\n" +
            "see|sees|saw|seen|seeing|1|видить\n" +
            "come|comes|came|come|coming|1|приходить"



    val ss = "1|child|children|ребенок|1|1|1\n" +
            "            2|student|students|студент|1|1|1\n" +
            "            3|engineer|engineers|инженер|1|1|2\n" +
            "            4|hero|heros|герой|1|1|2\n" +
            "            5|genius|geniuses|гений|1|1|2\n" +
            "            6|parent|parents|родитель|1|1|2\n" +
            "            7|teacher|teachers|учитель|1|1|2\n" +
            "            8|manager|managers|управляющий|1|1|2\n" +
            "            9|worker|workers|работник|1|1|2\n" +
            "            10|officer|officers|офицер|1|1|2\n" +
            "            11|director|directors|директор|1|1|2\n" +
            "            12|son|sons|сын|1|1|2\n" +
            "            13|leader|leaders|лидер|1|1|2\n" +
            "            14|animal|animals|животное|1|1|2\n" +
            "            15|doctor|doctors|доктор|1|1|2\n" +
            "            16|player|players|игрок|1|1|2\n" +
            "            17|boy|boys|мальчик|1|1\\\"\\t\\n\" +\n" +
            "            18|girl|girls|девушка|1|1\\\"\\t\\n\" +\n" +
            "            19|man|men|человек|1|1\\\"\\t\\n\" +\n" +
            "            20|woman|women|женщина|1|1\\\"\\t\\n\" +\n" +
            "            21|daughter|daughters|дочь|1|1\\\"\\t\\n\" +\n" +
            "            22|brother|brothers|брат|1|1\\\"\\t\\n\" +\n" +
            "            23|sister|sisters|сестра|1|1\\\"\\t\\n\" +\n" +
            "            24|dog|dogs|собака|1|1\\\"\\t\\n\" +\n" +
            "            25|cat|cats|кошка|1|1\\\"\\t\\n\""






    val ssss ="2|you|are|вы, ты|1|1|0|were|11\t\n" +
            "3|we|are|мы|1|0|0|were|11\t\n" +
            "4|they|are|они|1|0|0|were|11\t\n" +
            "5|he|is|он|0|1|1|was|21\t\n" +
            "6|she|is|она|0|1|1|was|21\t\n" +
            "7|it|is|это|0|1|1|was|21\t\n" +
            "1|i|am|я|0|1|0|was|1\t\n" +
            "8|the friends|are|те самые друзья|1|0|0|were|41\t\n" +
            "10|a dog|is|собака|0|1|1|was|41\t\n" +
            "10|the dogs|are| собаки|1|0|0|were|41\t\n" +
            "9|a boy|is|мальчик|0|1|1|was|41\t\n" +
            "9|the boys|are|мальчики|1|0|0|were|41\t\n" +
            "11|a girl|is|девушка|0|1|1|was|41\t\n" +
            "11|the girls|are|девушки|1|0|0|were|41\t\n" +
            "8|the friend|is|тот самый друг|0|1|1|was|41\t\n" +
            "8|friends|are|друзья|1|0|0|were|41|2\n" +
            "9|the boy|is|мальчик|0|1|1|was|41\t\n" +
            "10|the dog|is|собака|0|1|1|was|41\t\n" +
            "8|a friend|is|один друг|0|1|1|was|41\t\n" +
            "11|the girl|is|девушка|0|1|1|was|41\t\n" +
            "11|girls|are|девушки|1|0|0|were|41|2\n" +
            "9|boys|are|мальчики|1|0|0|were|41|2\n" +
            "10|dogs|are| собаки|1|0|0|were|41|2\n" +
            "12|a enemy|is|враг|0|1|1|was|41\t\n" +
            "12|the enemies|are|враги|1|0|0|were|41\t\n" +
            "12|the enemy|is|враг|0|1|1|was|41\t\n" +
            "12|enemies|are|враги|1|0|0|were|61|2\n" +
            "13|a monkey|is|обезьяна|0|1|1|was|61\t\n" +
            "13|the monkeys|are|обезьяны|1|0|0|were|61\t\n" +
            "13|the monkey|is|обезьяна|0|1|1|was|61\t\n" +
            "13|monkeys|are|обезьяны|1|0|0|were|61|2\n" +
            "14|a lover|is|любитель|0|1|1|was|61\t\n" +
            "14|the lovers|are|лубители|1|0|0|were|61\t\n" +
            "14|the lover|is|любитель|0|1|1|was|61\t\n" +
            "14|lovers|are|любители|1|0|0|were|61|2\n" +
            "15|a kid|is|ребенок|0|1|1|was|61\t\n" +
            "15|the kids|are|дети|1|0|0|were|61\t\n" +
            "15|the kid|is|ребенок|0|1|1|was|61\t\n" +
            "15|kids|are|дети|1|0|0|were|61|2\n" +
            "16|a human|is|человек|0|1|1|was|81\t\n" +
            "16|the humans|are|люди|1|0|0|were|81\t\n" +
            "16|the human|is|человек|0|1|1|was|81\t\n" +
            "16|humans|are|люди|1|0|0|were|81|2\n" +
            "17|a cat|is|кошка|0|1|1|was|81\t\n" +
            "17|the cats|are|кошки|1|0|0|were|81\t\n" +
            "17|the cat|is|кошка|0|1|1|was|81\t\n" +
            "17|cats|are|кошки|1|0|0|were|81|2"

        fun main () {
            for(i in 1..30){
                val phraseCreator:PhraseCreator = ToBePastQuestion(getRandomWordPack())
                printPhras(phraseCreator)
            }
        }

fun getRandomWordPack():WordPack{
    var verbs = getVerbs()
    val subjects = getSubjectObjectNoun().toList()
    val nouns = getNouns()
    return WordPack(subjects.shuffled()[0], verbs.shuffled()[0], nouns.shuffled()[0])
}

fun printPhras(phraseGenerator: PhraseCreator){
    val phrases = phraseGenerator.phrase
    println(phraseGenerator.tensAndType.name)
    phrases.forEach({print(it.ansverPart+ " " )})
    println()
    phrases.forEach({println("${it.qestionPart} - ${it.ansverPart}")})
    println("===========================")
}

