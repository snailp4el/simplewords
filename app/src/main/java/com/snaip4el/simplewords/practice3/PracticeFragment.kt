package com.snaip4el.simplewords.practice3

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import com.snaip4el.simplewords.App
import com.snaip4el.simplewords.R
import com.snaip4el.simplewords.ScreanContentVM
import com.snaip4el.simplewords.databinding.ActivityPractice2Binding
import com.snaip4el.simplewords.phraseGenerator.*
import javax.inject.Inject
import javax.inject.Singleton

private const val TAG = "PracticeFragment"

                        // если в конструкторе чтото есть поворот не работает
class PracticeFragment @Inject constructor(): Fragment(), PracticeContract.View {

    lateinit var binding: ActivityPractice2Binding

    @Inject
    lateinit var practicePresenter: PracticePresenter
    @Inject
    lateinit var screanContentVM: ScreanContentVM

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.activity_practice2, container, false)
        App.component.inject(this)

        practicePresenter.attach(this)
        binding.rootViewP2.setOnClickListener({practicePresenter.onScreenClick()})
        binding.next.setOnClickListener({practicePresenter.onScreenClick()})

        binding.lifecycleOwner = this//без этого не будет автообновлятся screanContentVM
        binding.viewmodel = screanContentVM



        return binding.rootViewP2
    }

    override fun showQuestion(string: String) {
        screanContentVM._question.postValue(string)
    }

    override fun showAnswer(string: String) {
        screanContentVM._answer.postValue(string)
    }

    override fun showTens(tens: Tens) {
        screanContentVM._tensAndType.postValue(tens)
    }

    override fun showAllPhrase(phraseCreator: PhraseCreator) {
        showQuestion(phraseCreator.getQuestionSring())
        showAnswer(phraseCreator.getAnsverSring())
        showTens(phraseCreator.tensAndType)
    }

}