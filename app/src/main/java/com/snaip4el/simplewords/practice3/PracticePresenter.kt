package com.snaip4el.simplewords.practice3

import android.util.Log
import com.snaip4el.simplewords.ISpeaker
import com.snaip4el.simplewords.phraseGenerator.*
import com.snaip4el.simplewords.task_selector.BaseFetcher
import com.snaip4el.simplewords.utils.PhraseCreatorUtils
import io.reactivex.rxjava3.schedulers.Schedulers
import java.util.*
import javax.inject.Inject
import javax.inject.Singleton

private const val TAG = "PracticePresenter"

@Singleton
class PracticePresenter @Inject constructor(val iSpeaker: ISpeaker): PracticeContract.Presenter {

    private lateinit var view: PracticeContract.View
    var tasks : List<Task> = listOf(Task.SIMPLE_FUTURE_POSITIVE)
    var queueOfCreators: Queue<PhraseCreator> = LinkedList()


    override fun onScreenClick() {
        Log.i(TAG, "onScreenClick()")
        makeAStep()
    }


    override fun attach(view: PracticeContract.View) {
        this.view = view
        BaseFetcher.getTasksFromDb().subscribeOn(Schedulers.computation()).observeOn(Schedulers.io()).subscribe({
            tasks = it
            iSpeaker.getSpeachStatusSubject().subscribe(){
                endOfSpeaking()
            }
            ready()
        })
    }

    private fun endOfSpeaking() {
        Log.i(TAG, "endOFspeaking")
    }

    private fun ready() {
        makeAStep()
    }

     fun makeAStep() {
        Log.i(TAG, "makeAStep() this hash ${this.hashCode()}")
        Log.i(TAG, "queueCreators size = $ ${queueOfCreators.size}")

        if (queueOfCreators.isEmpty()) {
            queueOfCreators = PhraseCreatorUtils.createNewQueue(tasks)
            queueOfCreators.peek().actionNeadToBeDone = PhraseCreator.ActionNeadToBeDone.answer
        }

        when (queueOfCreators.peek().actionNeadToBeDone){
            PhraseCreator.ActionNeadToBeDone.question -> questionStep(queueOfCreators.peek())
            PhraseCreator.ActionNeadToBeDone.answer -> answerStep(queueOfCreators.poll())
        }

    }

    private fun questionStep(phraseCreator: PhraseCreator) {
        view.apply {
            showAnswer(" ")
            showQuestion(phraseCreator.getQuestionSring())
            showTens(phraseCreator.tensAndType)
        }
        phraseCreator.actionNeadToBeDone = PhraseCreator.ActionNeadToBeDone.answer
        Log.i(TAG, "firstStep()")

    }
    private fun answerStep(phraseCreator: PhraseCreator){
        view.showAllPhrase(phraseCreator)
        iSpeaker.say(phraseCreator.getAnsverSring())
        phraseCreator.actionNeadToBeDone = PhraseCreator.ActionNeadToBeDone.doneWithIt
        Log.i(TAG, "seccondStep()")
    }

}