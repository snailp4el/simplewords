package com.snaip4el.simplewords.practice3

import com.ogulcan.android.mvp.app.ui.base.BaseContract
import com.snaip4el.simplewords.phraseGenerator.PhraseCreator
import com.snaip4el.simplewords.phraseGenerator.Tens

class PracticeContract {

    interface View: BaseContract.View {

        fun showQuestion(string: String)
        fun showAnswer(string: String)
        fun showTens(tens: Tens)
        fun showAllPhrase(phraseCreator: PhraseCreator)
    }

    interface Presenter: BaseContract.Presenter<PracticeContract.View> {
        fun onScreenClick()
    }

}